var db = null;

document.addEventListener('deviceready', function() {
	db = window.sqlitePlugin.openDatabase({
		name: 'my.db',
		location: 'default',
	});		
	db.transaction(function(tx) {
		tx.executeSql('CREATE TABLE IF NOT EXISTS lieu (nom TEXT, description TEXT,latitude TEXT,longitude TEXT, commentaire TEXT, media TEXT)');
	}, function(error) {
		console.log('Transaction ERROR: ' + error.message);
	}, function() {
		console.log('Create database OK');
		
		db.transaction(function(tx) {
			//tx.executeSql('INSERT INTO lieu VALUES ("colmar","centre",10, 12, "colmar centre ville", "video Exemple")');
			tx.executeSql('INSERT INTO article VALUES (?,?,?,?,?,?)', ["colmar","centre",10, 12, "colmar centre ville", "video Exemple"]);
		}, function(error) {
			console.log('Transaction ERROR: ' + error.message);
		}, function() {
			console.log('Populated database OK');
			
			remplirListe();
		});
		
	});
});

function remplirListe(){
	db.executeSql('SELECT rowid, * FROM lieu', [],
		function(res) {
			for (var i = 0; i < res.rows.length; i++) {
				console.log(res.rows.item(i).rowid+" "+res.rows.item(i).nom);
				//<li><a href="lieu.html" data-nom="Sucre" data-qty="500" data-unite="g">Sucre - 500 g</a></li>
				
				 (, TEXT,  TEXT,  TEXT,  TEXT,  TEXT,  TEXT)');
				var strhtml = '<li><a href="lieu.html" data-nom="';
				strhtml += res.rows.item(i).nom;
				strhtml += '" data-description="';
				strhtml += res.rows.item(i).description;
				strhtml += '" data-latitude="';
				strhtml += res.rows.item(i).latitude;
				strhtml += '" data-longitude="';
				strhtml += res.rows.item(i).longitude;
				strhtml += '" data-commentaire="';
				strhtml += res.rows.item(i).commentaire;
				strhtml += '" data-media="';
				strhtml += res.rows.item(i).media;
				

				strhtml += '" data-rowid="';
				strhtml += res.rows.item(i).rowid;
				
				strhtml += '">';
				strhtml += '</a></li>';
				
				//Ajouter la chaine à la liste
				$("#liste_courses").append(strhtml);
			}
			//Rafraichir graphiquement la liste
			$("#liste_courses").listview("refresh");
			
			//Ajouter la commande de clic
			$("#liste_courses a").click(clicListe);
		},
		function(error) {
			console.log('SELECT SQL statement ERROR: ' + error.message);
		}
	);
}

var ptrArticle = null;

function clicListe() {
	$("#nom_lieu").val($(this).data("nom"));
	$("#description_lieu").val($(this).data("description"));
	$("#latitude_lieu").val($(this).data("latitude"));
	$("#longitude_lieu").val($(this).data("longitude"));
	$("#commentaire_lieu").val($(this).data("commentaire"));
	$("#media_lieu").val($(this).data("media"));


	ptrlieu = $(this);
}

$("#btn_ajout").click(function(){
	//remise à zéro des champs pour ajouter un nouveau lieu
	$("#nom_lieu").val('');
	$("#description_lieu").val('');
	$("#latitude_lieu").val('');
	$("#longitude_lieu").val('');
	$("#commentaire_lieu").val('');
	$("#media_lieu").val('');
	
	ptrlieu = null;
});

$("#btn_valide").click(function(){
	if (ptrlieu !== null) {
		//On est en édition
		
		if ($("#nom_lieu").val().length>0) {
			if ($("#description_lieu").val().length>0) {
				if ($("#latitude_lieu").val().length>0) {
					if ($("#longitude_lieu").val().length>0) {
						if ($("#commentaire_lieu").val().length>0) {
							if ($("#media_lieu").val().length>0) {

				//Data
				$(ptrArticle).data("nom", $("#nom_lieu").val());
				$(ptrArticle).data("description", $("#description_lieu").val());
				$(ptrArticle).data("latitude", $("#latitude_lieu").val());
				$(ptrArticle).data("longitude", $("#longitude_lieu").val());
				$(ptrArticle).data("commentaire", $("#commentaire_lieu").val());
				$(ptrArticle).data("media_lieu", $("#media_lieu").val());
																				
				//Update la BDD
				db.executeSql("UPDATE article SET nom = ?, description = ?, latitude = ? longitude = ? commentaire = ? media = ? WHERE rowid = ?",
				[$("#nom_lieu").val(), $("#description_lieu").val(), $("#latitude_lieu").val(), $("#longitude_lieu").val(), $("#commentaire_lieu").val(), $("#media_lieu").val(), $(ptrlieu).data("rowid")],
				function(){ console.log("update OK"); },
				function(error) { console.log ("update Error "+error.message); });
				
				//Chaine affichée
				var strResult = $("#nom_lieu").val()+" - ";
				var strResult = $("#description_lieu").val()+" - ";
				var strResult = $("#latitude_lieu").val()+" - ";
				var strResult = $("#longitude_lieu").val()+" - ";
				var strResult = $("#commentaire_lieu").val()+' ';
				var strResult = $("#media_lieu").val();
				
				$(ptrlieu).html(strResult);
				
				//Changer de page
				$.mobile.changePage("index.html");
							} else {
								alert("Pas de media !");
							}
						} else {
							alert("Pas de commentaire !");
						}
					} else {
						alert("Pas de longitude !");
					}
				} else {
					alert("Pas de latitude !");
				}	
			} else {
				alert("Pas de description !");
			}
		} else {
			alert("Pas de nom !");
		}
		
	} else {
		//Ajout
		
		if ($("#nom_lieu").val().length>0) {
			if ($("#description_lieu").val().length>0) {
				if ($("#latitude_lieu").val().length>0) {
					if ($("#longitude_lieu").val().length>0) {
						if ($("#commentaire_lieu").val().length>0) {
							if ($("#media_lieu").val().length>0) {
				//Ajout dans BDD
				db.executeSql("INSERT INTO article VALUES(?, ?, ?)",
				[$("#art_nom").val(), $("#art_qty").val(), $("#art_unite").val()],
				function(){ console.log("Insert ok"); },
				function(error){ console.log("insert error"+error.message); });
				
				//Flush de la liste
				$("#liste_lieu").empty();
				
				//Remplissage de la liste
				//Synchronisation de la liste
				//Ajout du clic
				remplirListe();
				
				//Changer de page
				$.mobile.changePage("index.html");
							} else {
								alert("Pas de media !");
							}
						} else {
							alert("Pas de commentaire !");
						}
					} else {
						alert("Pas de longitude !");
					}
				} else {
					alert("Pas de latitude !");
				}	
			} else {
				alert("Pas de description !");
			}
		} else {
			alert("Pas de nom !");
		}
	}
	
	
	
});

 // onSuccess Callback
    //   This method accepts a `Position` object, which contains
    //   the current GPS coordinates
    //
    function onSuccess(position) {
        var element = document.getElementById('geolocation');
        element.innerHTML = 'Latitude: '  + position.coords.latitude      + '<br />' +
                            'Longitude: ' + position.coords.longitude     + '<br />' +
                            '<hr />'      + element.innerHTML;
    }

    // onError Callback receives a PositionError object
    //
    function onError(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }

    // Options: throw an error if no update is received every 30 seconds.
    //
    var watchID = navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 30000 });















